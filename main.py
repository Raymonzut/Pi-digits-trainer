from pi import pi_digits


def getDigits(amount):
    assert(amount < len(pi_digits), 'Not enough digits loaded')
    return pi_digits[:amount]


def checkValid(index):
    """Returns length of the numbers you got right as a sequence"""

    userInput = input()

    numGood = 0

    for i in range(len(userInput)):
        if userInput[i] == pi_digits[index + i]:
            numGood += 1
        else:
            break
    if numGood != len(userInput):
        print('Not quite there, try again')
        return checkValid(index)
    else:
        print('Good')

    return numGood


def main():
    print(20 * '\n' + 'Welcome the pi digit trainer')

    i = 0
    digitsTotal = int(input('How much digits?'))

    while i < digitsTotal:
        i += checkValid(i)
    print('Thanks for playing')


if __name__ == '__main__':
    main()
